// Створи клас, який буде створювати користувачів з ім'ям та прізвищем. Додати до класу метод для виведення імені та прізвища

let div1 = document.getElementById('task1');

class User {
    constructor(name, surname) {
        this.name = name;
        this.surname = surname;
    }
    showUser() {
        div1.innerText = (`Ім'я: ${this.name} Прізвище: ${this.surname}`)
    }
};

let user1 = new User("Вася", "Пупкін");
user1.showUser();

//Створи список, що складається з 4 аркушів. 
// Використовуючи джс зверніся до 2 li і з використанням навігації по DOM дай 1 елементу синій фон, а 3 червоний

const [...li] = document.querySelector("ul").children;
li[1].classList.add("blue");
li[2].classList.add("red");

// Створи див висотою 400 пікселів і додай на нього подію наведення мишки. 
// При наведенні мишки виведіть текст координати, де знаходиться курсор мишки

const divMouse = document.getElementById("task3");

let mouseOn = (e) => {
    e.target.innerHTML = (`X: ${e.clientX} Y: ${e.clientY}`)
}
divMouse.addEventListener("mouseover", mouseOn);

let mouseOut = (e) => {
    e.target.innerHTML = ('')
}
divMouse.addEventListener("mouseout", mouseOut);

// Створи кнопки 1,2,3,4 і при натисканні на кнопку виведи інформацію про те, яка кнопка була натиснута
const [...buttons] = document.querySelector(".buttons").children;

let getButton = (e) => {
    e.target.classList.add("green");
    console.log(e.target);
}

buttons.forEach(item => {
    item.addEventListener("click", getButton)
});

// Створи див і зроби так, щоб при наведенні на див див змінював своє положення на сторінці

const div5 = document.getElementById("task5");

div5.onmouseover = function() {
    let start = Date.now();

    let timer = setInterval(function() {
      let timePassed = Date.now() - start;

      div5.style.left = timePassed / 3 + 'px';

      if (timePassed > 2000) clearInterval(timer);

    }, 20);
  }

// Створи поле для введення кольору, коли користувач вибере якийсь колір, зроби його фоном body
let backgroundColor = document.getElementById("backgroundcolor");
let start = document.getElementById("start");

let getColor = () => {
    let colors = backgroundColor.value;
    document.body.style.backgroundColor = colors;
}
start.addEventListener("click", getColor);

//Створи інпут для введення логіну, коли користувач почне вводити дані в інпут виводь їх в консоль
let inputLogin = document.getElementById("login")

let getLogin = (e) => {
    console.log(e.target.value)
}
inputLogin.addEventListener("input", getLogin);

// Створіть поле для введення даних у полі введення даних виведіть текст під полем
let inputText = document.getElementById("text"),
p = document.createElement("p");
inputText.after(p);

let getText = () => {
    p.innerText = inputText.value;
}
inputText.addEventListener("input", getText)
