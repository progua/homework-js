//Створіть програму секундомір.
let divDisplay = document.getElementById(("display")),
    btnstop = document.getElementById("btnstop"),
    start = document.getElementById("btnstart"),
    reset = document.getElementById("btnreset"),
    sec = 0,
    min = 0,
    hrs = 0,
    intervalTaiming,
    flagStart = 0;

const stopwatch = () => {
    sec++;
    if (sec >= 60) {
        sec = 0;
        min++;
        if (min >= 60) {
            min = 0;
            hrs++;
        }
    }
    document.getElementById("hour").textContent = taiming(hrs);
    document.getElementById("minutes").textContent = taiming(min);
    document.getElementById("seconds").textContent = taiming(sec);
};

const taiming = (tim) => {
    if (tim < 10) {
        return "0" + tim;
    }
    return tim;
};

const pressStart = () => {
    if (flagStart == 0) {
        divDisplay.classList.remove("dispaly-color");
        divDisplay.classList.remove("red");
        divDisplay.classList.remove("silver");
        divDisplay.classList.add("green");
        intervalTaiming = setInterval(stopwatch, 1000);
        flagStart = 1;
    }
};
const pressStop = () => {
    divDisplay.classList.remove("dispaly-color");
    divDisplay.classList.remove("green");
    divDisplay.classList.remove("silver");
    divDisplay.classList.add("red");
    clearInterval(intervalTaiming);
    flagStart = 0;

};

const pressReset = () => {
    divDisplay.classList.remove("dispaly-color");
    divDisplay.classList.remove("green");
    divDisplay.classList.remove("red");
    divDisplay.classList.add("silver");
    document.getElementById("hour").textContent = "00";
    document.getElementById("minutes").textContent = "00";
    document.getElementById("seconds").textContent = "00";
    sec = 0;
    min = 0;
    hrs = 0;
};

btnstop.onclick = pressStop;
start.onclick = pressStart;
reset.onclick = pressReset;

//Реалізуйте програму перевірки телефону
const fieldInput = document.createElement("input");
const fieldButton = document.createElement("button");
const container = document.querySelector(".telephone");
const divError = document.createElement("div");

fieldInput.placeholder = "Введіть номер телефону у форматі 000-000-00-00";
fieldButton.innerText = "Зберегти";
fieldInput.classList.add("input");
divError.innerText = "Невірний номер!"
container.append(fieldInput);
container.append(fieldButton);

const telephone = () => {
const pattern = /\d\d\d-\d\d\d-\d\d-\d\d/;
const number = fieldInput.value;
if (pattern.test(number)) {
    window.location.href = "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg";
} else 
container.prepend(divError);
}

fieldButton.onclick = telephone;

// Слайдер
let imageIndex = 0;
const image = document.querySelector('.slidershow .img-wrap-show img'),
showImages = [
"https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg",
"https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg",
"https://naukatv.ru/upload/files/shutterstock_418733752.jpg",
"https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg"
];
const slideShow  = () => {
    image.setAttribute('src', showImages[imageIndex]);
    imageIndex++;
    setTimeout(slideShow, 3000);
    if (imageIndex === showImages.length)  imageIndex = 0;
}
slideShow ();


//Слайдер з перелистуванням
const myImage = document.querySelector('.slider .img-wrap img'),
images = [
"https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg",
"https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg",
"https://naukatv.ru/upload/files/shutterstock_418733752.jpg",
"https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg"
];
let imageIndex1 = 1,
imageIndex2 = images.length - 1,
next = document.getElementById("next"),
previous = document.getElementById("previous");
const slideShowNext  = () => {
    myImage.setAttribute('src', images[imageIndex1]);
    imageIndex1++;
    if (imageIndex1 === images.length)  imageIndex1 = 0;
}
const slideShowPrevious  = () => {
    myImage.setAttribute('src', images[imageIndex2]);
    imageIndex2--;
    if (imageIndex2 === -1)  imageIndex2 = images.length -1;
}
previous.onclick = slideShowPrevious;
next.onclick = slideShowNext;
