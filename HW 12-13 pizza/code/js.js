document.addEventListener("DOMContentLoaded", () => {

    const price = {
        small: 100,
        mid: 125,
        big: 150,
        ketchup: 15,
        bbq: 20,
        rikotta: 25,
        cheese: 10,
        feta: 15,
        mozarella: 20,
        telya: 25,
        tomatoes: 15,
        mushrooms: 15
    };

    const pizza = {
        size: null,
        sauce: [],
        nameSauce: [],
        topping: [],
        nameTopping: [],
        totalPrice: 0,
    };

    const pizzaSize = document.getElementById("pizza");
    pizzaSize.addEventListener('click', (e) => {
        pizza.size = e.target.value;
        yourOrder();
    })

    const totalPrice = () => {
        if (pizza.totalPrice != 0) {
            pizza.totalPrice = 0;
        }

        switch (pizza.size) {
            case "small":
                pizza.totalPrice += price.small;
                break;
            case "mid":
                pizza.totalPrice += price.mid;
                break;
            case "big":
                pizza.totalPrice += price.big;
                break;
        }

        pizza.sauce.forEach(el => {
            switch (el) {
                case "sauceClassic":
                    pizza.totalPrice += price.ketchup;
                    break;
                case "sauceBBQ":
                    pizza.totalPrice += price.bbq;
                    break;
                case "sauceRikotta":
                    pizza.totalPrice += price.rikotta;
                    break;
            }
        })

        pizza.topping.forEach(el => {
            switch (el) {
                case "moc1":
                    pizza.totalPrice += price.cheese;
                    break;
                case "moc2":
                    pizza.totalPrice += price.feta;
                    break;
                case "moc3":
                    pizza.totalPrice += price.mozarella;
                    break;
                case "telya":
                    pizza.totalPrice += price.telya;
                    break;
                case "vetch1":
                    pizza.totalPrice += price.tomatoes;
                    break;
                case "vetch2":
                    pizza.totalPrice += price.mushrooms;
                    break;
            }
        })
    }

    const showPrice = () => {
        document.querySelector(".price > p").innerHTML = `Ціна: ${pizza.totalPrice}`;
    }


    const showSauces = () => {
        document.querySelector(".sauces > p").innerHTML = `Соуси: ${pizza.nameSauce.join(", ")}`;
    }


    const showToppings = () => {
        document.querySelector(".topings > p").innerHTML = `Топінги: ${pizza.nameTopping.join(", ")}`;
    }


    const yourOrder = () => {
        totalPrice();
        showPrice();
        showSauces();
        showToppings();
    }


    const dragAndDrop = () => {
        const pizzaStart = document.querySelector(".table");

        function dragStart(e) {
            const item = e.target.dataset.key;
            if (item === "sauce") {
                if (!pizza.nameSauce.includes(e.target.alt)) {
                    pizza.nameSauce.push(e.target.alt);
                    pizza.sauce.push(e.target.id);
                }
            } else {
                if (!pizza.nameTopping.includes(e.target.alt)) {
                    pizza.nameTopping.push(e.target.alt);
                    pizza.topping.push(e.target.id);
                }
            }
            e.dataTransfer.effectAllowed = 'move';
            e.dataTransfer.setData("img", this.attributes.src.textContent);
        }

        const dragEnd = (e) => {
            pizzaStart.classList.remove("hovered");
            if (e.preventDefault) {
                e.preventDefault();
            }
            if (e.stopPropagation) {
                e.stopPropagation();
            }
        }

        function dragOver(e) {
            if (e.preventDefault) {
                e.preventDefault();
            }
            if (e.stopPropagation) {
                e.stopPropagation();
            }
        }

        function dragEnter(e) {
            pizzaStart.classList.add("hovered");
            if (e.preventDefault) {
                e.preventDefault();
            }
            if (e.stopPropagation) {
                e.stopPropagation();
            }
        }


        function dragLeave(e) {
            pizzaStart.classList.remove("hovered");
            if (e.preventDefault) {
                e.preventDefault();
            }
            if (e.stopPropagation) {
                e.stopPropagation();
            }
        }


        function dragDrop(e) {
            if (e.preventDefault) {
                e.preventDefault();
            }
            if (e.stopPropagation) {
                e.stopPropagation();
            }

            const itemPic = document.createElement("img");
            itemPic.setAttribute("src", e.dataTransfer.getData("img"));
            pizzaStart.append(itemPic);

            yourOrder();
        }


        pizzaStart.addEventListener('dragover', dragOver);
        pizzaStart.addEventListener('drop', dragDrop);
        pizzaStart.addEventListener('dragenter', dragEnter);
        pizzaStart.addEventListener('dragleave', dragLeave);


        const ingridients = document.querySelectorAll('.ingridients .draggable');
        ingridients.forEach(item => {
            item.addEventListener('dragstart', dragStart);
            item.addEventListener('dragend', dragEnd);
        })
    }
    dragAndDrop();

    let [...allInputs] = document.querySelectorAll(".input_date");

    const validate = (target) => {
        switch (target.id) {
            case "name": return /[a-zа-ї]+/i.test(target.value);
            case "phone": return /^\+380\d{9}$/.test(target.value);
            case "email": return /\b\w+@[a-z]+\.[a-z]{2,4}\b/g.test(target.value);
        }
    }

        allInputs.forEach((e) => {
        let p = document.createElement("p");
        e.addEventListener("change", (event) => {
            if (!validate(event.target)) {
                event.target.classList.add("error");
                event.target.classList.remove("success");
                e.after(p)
                p.innerText = 'Невірні дані'
            } else  {
                p.remove()
                event.target.classList.remove("error");
                event.target.classList.add("success");

            }

        })
    });

    let saveButton = document.getElementById("submit");


    saveButton.addEventListener("click", () => {
        let validateRez = allInputs.map(function (element) {
            return validate(element);
        });

        if (!validateRez.includes(false)) {
            location.href = "./thank-you.html"
        }
        else {
            alert("Перевірте дані")
        }
    })


    const btnReset = document.getElementById("resetButton");

    btnReset.addEventListener("click", () => {

        pizza.size = null;
        pizza.sauce = [];
        pizza.nameSauce = [];
        pizza.topping = [];
        pizza.nameTopping = [];
        pizza.totalPrice = 0;

        const item = document.querySelectorAll('.table img');

        item.forEach(item => {
            if (!item.alt[0]) {
                item.remove();
            }
        });


        allInputs.forEach((e) => {
            e.value = ""
        });
        yourOrder();
    })


    const discount = document.getElementById("banner");
    discount.addEventListener("mousemove", (e) => {
        banner.style.right = Math.random() * (document.documentElement.clientWidth - e.target.offsetWidth) + 'px';
        banner.style.bottom = Math.random() * (document.documentElement.clientHeight - e.target.offsetHeight) + 'px';
    })
})



