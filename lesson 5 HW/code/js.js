// Создать объект "Документ", в котором определить свойства "Заголовок, тело, футер, дата". Создать в объекте вложенный объект - "Приложение". Создать в объекте "Приложение", вложенные объекты, "Заголовок, тело, футер, дата". Создать методы для заполнения и отображения документа.
let myDocument = {
    header: "Заголовок",
    body: "Тело",
    footer: "Футер",
    date: "Дата",
    app: {
        header: "",
        body: "",
        footer: "",
        date: ""
    },
    fill: function() {
        myDocument.app.header = prompt("Введіть значення для header");
        myDocument.app.body = prompt("Введіть значення для body");
        myDocument.app.footer = prompt("Введіть значення для footer");
        myDocument.app.date = prompt("Введіть значення для date");
    },
    show: function() {
        document.getElementById("mydocument").innerHTML = `Ваш документ: <br> Header: ${this.header}  <br> Body: ${this.body}  <br> Footer: ${this.footer} <br> Date: ${this.date}`;
        document.getElementById("myapp").innerHTML = `Ваші значення у додатку: <br> ${Object.values(myDocument.app).join("<br>")}`;
    
}
};
myDocument.fill();
myDocument.show();

// Создайте объект заработных плат obj. Выведите на экран зарплату Пети и Коли.
var obj = {'Коля':'1000', 'Вася':'500', 'Петя':'200'};
document.getElementById("task1").innerHTML = `ЗП Пети: ${obj.Петя}, ЗП Коли: ${obj.Коля}`;
// Дана строка вида 'var_text_hello'. Сделайте из него текст 'VarTextHello'

let str = ("var_text_hello");
function capitalize(str) {
    let a = str.split('_');
    let up = [];
    for(let i = 0;i < a.length;i++) {
    let res = a[i][0].toUpperCase() + a[i].slice(1, a[i].length);
    up.push(res);
    }    
    document.write(`${up.join("")} <br>`);
}
capitalize(str);
// Если переменная a больше нуля - то в ggg запишем функцию, которая выводит один !, иначе запишем функцию, которая выводит два !
let a = parseInt(prompt("Введіть число"));
let ggg;
if (a > 0) {
  ggg = function () {
    alert(`Один!`);
  }
  }
  else {
    ggg = function () {
     alert(`Два!`);
    }
  }
  ggg();
  // Функция ggg принимает 2 параметра: анонимную функцию, которая возвращает 3 и анонимную функцию, которая возвращает 4. Верните результатом функции ggg сумму 3 и 4.
  
  function ggg1 (c, d) {
    return c() + d();
  }
  let fn3 = function() {
    return 3;
  }
  let fn4 = function() {
    return 4;
  }
  console.log(ggg1(fn3, fn4));
  //Сделайте функцию, которая считает и выводит количество своих вызовов
  let num = 0;
function counterFunction () {
     num++
    document.write(`Викликано ${num} раз <br>`)
}
counterFunction ()
counterFunction ()
counterFunction ()
counterFunction ()
// Напишите функцию isEmpty(obj), которая возвращает true, если у объекта нет свойств, иначе false
let obj2 = {};
function isEmpty() {
    for (let key in obj2) {
        if (obj2.hasOwnProperty(key)) {
          return false;
        }
      }
      return true;
};



