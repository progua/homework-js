const ADD_MESSAGE = 'ADD-MESSAGE';

let initialState = {
    dialogsData: [
        { id: 1, name: 'Nastia' }, { id: 2, name: 'Vlad' }, { id: 3, name: 'Oleg' }, { id: 4, name: 'Aleksey' }
    ],
    messageData: [
        { id: 1, message: 'Hi', img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Calico_tabby_cat_-_Savannah.jpg/1200px-Calico_tabby_cat_-_Savannah.jpg', user: 'Me' },
        { id: 2, message: 'How are you?', img: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxATEhAQEBIVFRUVFhYTGBcVFxgVFRcaFRgWGhUXFRYYHSkgGB8lHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OFxAQGi0lHSUtLS0tLy0tLS0tKy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSstLS0tKy0tLf/AABEIAMgA/AMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAQUDBAYCB//EADkQAAIBAwIDBQcDAwIHAAAAAAABAgMRIQQxBRJBUWFxgZEGIqGxwdHwE0LhMnLxB7IUFSMkYpKi/8QAGQEBAAMBAQAAAAAAAAAAAAAAAAECAwQF/8QAIxEBAQACAgICAgMBAAAAAAAAAAECEQMhEjFBURNhIjJxBP/aAAwDAQACEQMRAD8A+4gAgAAAAAAAAAAAAAAAAAQSAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEEgCGSQwAAAEnlsKaA9AhMkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAh9oEmGrqoxw3kreKcT5cJlMtbdt+JhnzzHqNsOK3uugr8TS2RrrisnjCKbUauxhepwrP8/LGF579tpwxZ6jXtt5MdPiMrpFPLUXdk9/5ZMatmn24X53mX5Mr8tPxyOpocS2uWNPUxZxqr2fe/zBt0derXvhG2HPZ7Y5cU+HWqSBQUOKeOfE3qXFqf7pKPezox5caxvHlFiSeITTV4tNdx6NWYSQSAAAAAAAAAAAAAAAAAAIYAAASa2tqcsW0RqdbCG7z2Lcq9dq24t/iM885JpfHC3tzevrtya6N/n0MNOph36fLBj1X9S72/z4niM8NnmW7r0ZNR7rVL79UYnV2jfrfyMNRt9e2PwRNSKurb5+dvuVS2qeM9MESqr3ZPZX/LETzj87/qcR7d8dnp1GlDeeXbe3RfnYa8WFzuopnZJuuqrcTy/p8kbugq8yy3Y+YcF41Ukmm232X+h1Gg4hNpLmas7WSfXtL58Xipjlt11fXJWvJKN93drzPENVfKkpXeLW9MHP06j3y3vvv4NvBZ0d02nft6+vXxKNNL7ScRnFuVOV+2MnbbwOq4ZxGNVdklujhYVl+5yV9ndfG+fNXN3QV3CSkpXfha6+vl6G3FyXG/pjyccsd2DDo9QpxUkZjul24rNJBAAkEACQQAJAAAAAAAAIBi1Vbki5dgt0FevGCvJ2KPivHH/TTw34X9Cp1XEHVnJ3sljGfDuKniGoaxG3M+rz5vBxcnPb1HXx8EndZ6utSlmXPL+73V426lnQr80G7O+13hPwOQp5ly81+9f1SfW/YjseGaK1NKxljGufSo1MM57/8AJq1ZYt5v/P5sXOsoFTUSz3v0aMrF5WGOLrsu/h/BMpO91249Uj3LZ9svqrfc2NNpk1HwS9N38F6jSdtVXtHN7fHt+i8z5v8A6laT/uIVZN25Y2sr5X58z6pU06urLKWE9/7mun52GnreCU60k6kU2sLu+5rwZeGW2XJ/KafKvYzQylVb5Zcr6vHYdfxHhlWl79O77UtzrqPCY00lCKXobdPTXw16mufJ5XauOOoqPZagq9L9SSaeItNWaa3+SLLUaCXf0z+dxu6OMNPGpjdp2RXar2tpK65WnaTTe10rxv6fIz8JVvOta0oO2N72aa/9WtizpVFJKzWc2eV5/c80Ndp9R7sLN2vZ73NOVJ0mvPf5XZGrFtyx0/s9reWbpywnsdMfPKdV87aaxZ2as7dTs+Ea+NSKzk6+HPrTk5cNdrAAG7EAAAAASAAAAAAACCm9pNQlBQu1fOOwuJOybeyycZx3WKUpP08F08cmXNlrFrxY7yVVbUb8vupYT+bX4yorOzte7e/M/p187GXWal9FeT2TeF3u23l9M5+E8KUnzS3by7b/AG/nxOGTbtt0tfZ/hitzzs+z75L/AFDUYkaeKUUk9lbYrOJ6jomrdb4v4GtnjGH9q162rhfMu7uyaWph3pZ9fz6menpOdWbuvA19f7i5XhbZMdNf8a8KfMrNpfNdMCrrkpWgpNppWWbLt7/5IoVbyUF6/a/VmeVCPJLllZ81m4+9t0+BFiZWOjqW3UsnePf/AFO21zZoalJRdRcrfm/LuKXUtRUo3tG6dovL6vbPoKWrtJKTk08xSi8W7bkws26vmurxfxRkpO2/0KjRcQqTdrXiu67+BvSqq127ef0Ls9NjXTg4vPx+xw3GdDFu6e/e2dNqKqf7pPu2XyKLXuMrq2exP/AtWx6U/D6roVY1FLKadu1bbH0GtKnqIc0GttvFZuvM+d1KVpbr/d8S34Tq502pc3w38iZl8Us+VulyW3TVlvlb2a/Ovgbmg4g6c0s3bT7n3rseCalOFaLnB2k1leWfojSTaspJ3Wz6ru7+nwJnXpX2+gcO4nGolnL9GWBw/D9S1bm674sdfw/UqccbrD+jOvjz8o5s8PGtkAGjMAAEgAAAAAIIbtlgVvHtRy0+W9m/kcFxDUpN28l4dX/Je+0ev5ptLw8ihlpLLnqtRUs5f51t8Ow4eW+WXTt4p449sHD9LztzeI9r3lbv7O71Ntcco88aFJ3aeX0Xxycn7Ue1qxR039K3fjay9F8So9m6DlU96TXVpbvx7PMY4yJvb7VSk3FZKjiU4qVnZ/3ZQ4euVRasvLH8nviFKTd01bre25OXcUx6rxS1PYo+TRW8cqRhBtyTis4aco+Gcoy1qltnna+U/XJrVFBqz97xvfwV99zPW145Kr7QrnVOjP35Yc2n7kdr+NkzqNDWhToxhRXNFLftva7u8tde852WgpR1HM7Lnxl9U7r/ACX8NNjF33vL9Xe/n6lssNekyxiqTfNfmd72xH4fyYk5vNpWv1d7LuNyNB5eUrt3v2dxjr6fm91Skmu1vNzNZk02onCDu1e9sb+hmoyvmUm32fjNRadQXvPmk3hvHqauoqzi7yWe5LmfcpbNfQtFasOIzVrqpKNum6fdlfI5z/iZOTUlK3Rv6GSho6td3TVsSdr2klta+Wr9C/pcNV05KLklbOb91iLYmRT0FHqrX6u2fH7m7T07tfb87Szeni3ZxSxukum+xo6+hJL3LuPp+eZE7K2uGPlkmmzsNJw6FaN2kvA4zh89rHX+zOo95wNeLW9Vlyb1uLSlwSmja0ejVPmtszaB2TCT05blb7AAWQAACQyLsi7AkEZF2BJVcf1qpwSva/0LS7Kbj/BZaizU+W3S17lc9+PS2GvLtxd/1JuUnaCTk/DpHzz8DhPbLjc69ZxTtCGIqO3T1yfSoezGri2oypyhK91Ju7vK6/b0uz1o/wDTqi5xnXhTss8sOZX7pPqjmx48vp0Xkx+3yz2d9ltXq5f9CGFiVSWIR2uk+r8P5Ozp8Cp6FqN1OXVvF2ut30Pq+n08YRUIRjGKVko4S8EkUfFvZiNd83Pyu/Ze/wATS8XSk5t39OTo8QTleTuktldJeLN6GrU4u0Hbu+htan2GnKKitRZd8ZO//wBllS4X+jTjC9+Xql9DO4ZSLeeN9OZraWq2uWHq0krGCr7PamSa/VhCT2STnv2vBe0ny1JKSldJuKez269DapZcamVy5cfJqz/OhlKva+Vca4BqdNJOWohUlL9sotPPRXZt8H4tVg+WrDH/AIvmXTc7jiugnVkqj5fd/a7q92t33b2wmyk03CpuUuaPLFt2s8XXZbz9Rc76TqN6lq6bV27J+niZ1Ki/3Ra7OzsNaXs87YeDZ0/B5J5XZ/P1I7+jr7UvEXabSyuzKx+dTG+GynyJuyeFn3s9H2rv9Tq/+UpLCTa2UnZeqTsauj4VX/UlOrKmoppxUOa6s+rZPjZ3Uec+EaPQyS5Fbmta6SXj4WXzDotycNm3l73Xau8u6MFFynn3k3Z/t2tZeWxWUtK4VJ1d+e3L2rdtZ8b+bKZYxaZ1SSrunKUXG3Ldc2bb22T8Dc0q/Vi+Z2xZpY9Dzr6FSUowlFe9Lmut0o2ssY7u8tqXCfdah7t+3ZE447vRllNduaoUeWbS2L3g9Zwmn6mbSezFXF6kL9tn9iyp+zck7/qL0ZrOPPe5Gd5MfW3Q05XSa6now6WlKMVFu9uplyds9OWpBGRkCQRkZAkAEoAAAAAEWJAAMAADFXp3RlBFg57VxmrtWv0vt5lZS4g1dVIyUrdFi/Wx1WooXKutw9M4+Xhu94ujDknyqnWlLEb2a7/vc29LprLKNyGnSMnKRx8Fl3kZ8u+oxRiSzJyEqkdXix2wtHh07qxuRonpUSLjv2TLStdGXTKK/XcNnVau2rYx2HRqkSqRlf8AnlaTlsUmg4U1ZSz49PD4F1TppWRkUD0ol+PimCuWdyeqSNhGGCMyNYzSSQCyQABAAAAJASgEgCASAIBICEAkAQCQBDMNSkjOLAaE6R5VM3nAj9MjSWoqZ7VM2FTJ5BoYVA9KBlUSUhpDFyE8hlA0lh5CVAyiw0PKiTYkEiASAIBICEAkAAAEgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD//2Q==', user: "Nastia" }
    ]
}

const dialogsReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_MESSAGE:

            let message = {
                id: 3,
                message: action.messageText,
                img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Calico_tabby_cat_-_Savannah.jpg/1200px-Calico_tabby_cat_-_Savannah.jpg',
                user: "Me"
            }

            return {
                ...state,
                messageData: [...state.messageData, message]
            };

        default: return state
    }
}

export const addMessageActionCreate = (message) => ({ type: ADD_MESSAGE, messageText: message });

export default dialogsReducer