const ADD_POST = 'ADD-POST';

let initialState = {
    postData: [
        { id: 1, message: "It's my B-Day today", like: 15 },
        { id: 2, message: "What's up?", like: 4 },
        { id: 3, message: "It's my first post", like: 5 },
    ]
}

const profileReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_POST:
            let post = {
                id: 5,
                message: action.postText,
                like: 0
            }
            return {
                ...state,
                postData: [...state.postData, post]
            };

        default: return state

    }
}

export const addPostActionCreate = (text) => ({ type: ADD_POST, postText: text });

export default profileReducer