import './App.css';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Header from './components/Header/Header';
import Profile from './components/Profile/Profile';
import News from './components/News/News';
import Music from './components/Music/Music';
import Settings from './components/Settings/Settings';
import DialogsContainer from './components/Dialogs/DialogsContainer';
import NavContainer from './components/Navbar/NavContainer';
import UsersContainer from './components/Users/UsersContainer';



function App(props) {
  return (
    <BrowserRouter>
      <div className='app-wrapper'>
        <Header></Header>
        <NavContainer></NavContainer>
        <main className="app-wrapper-content">
          <Routes>
            <Route path='/profile' element={<Profile></Profile>}></Route>
            <Route path='/dialogs/*' element={<DialogsContainer></DialogsContainer>}></Route>
            <Route path='/news' element={<News></News>}></Route>
            <Route path='/music' element={<Music></Music>}></Route>
            <Route path='/settings' element={<Settings></Settings>}></Route>
            <Route path='users' element={<UsersContainer></UsersContainer>}></Route>
          </Routes>
        </main>
      </div>
    </BrowserRouter>
  );
}

export default App;

