import { connect } from 'react-redux';
import { followAC, setUsersAC, UNfollowAC } from '../../redux/users-reducer';
import Users from './Users'


let mapStateToProps = (state) => {
  return {
    users: state.usersPage.users
  }
};

let mapDispatchToProps = (dispatch) => {
  return {
    follow: (userID) => {
      dispatch(followAC(userID))
    },

    unfollow: (userID) => {
      dispatch(UNfollowAC(userID))
    },
    
    setUsers: (users) => {
      dispatch(setUsersAC(users))
    }
  }

}

export default connect(mapStateToProps, mapDispatchToProps)(Users);