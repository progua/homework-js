import s from './Users.module.css'

const Users = (props) => {
  if (props.users.length === 0) {
    props.setUsers(
      [
        {
          name: 'Lena', id: 1, avatar: 'https://sib.fm/storage/article/August2021/LDbBKOswklzvDOCD1W5x.jpg',
          location: { country: 'Belarus', city: 'Minsk' }, status: "What's going on", followed: false,
        },
        {
          name: 'Oleg', id: 2, avatar: 'https://sib.fm/storage/article/August2021/LDbBKOswklzvDOCD1W5x.jpg',
          location: { country: 'Ukraine', city: 'Kyiv' }, status: "Oh my...", followed: true,
        },
        {
          name: 'Saniya', id: 3, avatar: 'https://sib.fm/storage/article/August2021/LDbBKOswklzvDOCD1W5x.jpg',
          location: { country: 'Poland', city: 'Kralow' }, status: "I'm the best", followed: false,
        }
      ]
    )
  }

  let usersElement = props.users.map(user => <div key={user.id} className={s.users_body}>
    <div className={s.avatar}>
      <div><img src={user.avatar}></img></div>
      <div className={s.button}>{
        user.followed ? <button onClick={() => { props.unfollow(user.id) }}>Unfollow</button> :
          <button onClick={() => { props.follow(user.id) }}>Follow</button>
      }
      </div>
    </div>
    <div className={s.user_info}>
      <div className={s.user_status}>
        <div>{user.name}</div>
        <div>{user.status}</div>
      </div>
      <div className={s.user_location}>
        <div>{user.location.country}</div>
        <div>{user.location.city}</div>
      </div>
    </div></div>
  )

  return (
    <div>
      <div className={s.header}><h2>Users</h2></div>
      {usersElement}
    </div>
  )
}

export default Users;