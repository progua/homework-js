import s from './Nav.module.css';
import Friends from './Friends/Friends';
import { NavLink } from "react-router-dom";


const Nav = (props) => {

  let state = props.navBar;

  let friendElement = state.friends.map(friend => <Friends key={friend.id} name={friend.name} avatar={friend.avatar}></Friends>)

  return (
    <nav className={s.nav}>
      <ul>
        <li><NavLink to='/profile'>Profile</NavLink></li>
        <li><NavLink to='/dialogs'>Messages</NavLink></li>
        <li><NavLink to='/news'>News</NavLink></li>
        <li><NavLink to='/music'>Music</NavLink></li>
        <li><NavLink to='/settings'>Setings</NavLink></li>
        <li><NavLink to='/users'>Find Users</NavLink></li>
        <li><NavLink to='/friends'>Friends</NavLink></li>
      </ul>
      <div className={s.friends}>
      {friendElement}
      </div>
    </nav>
  )
}

export default Nav;