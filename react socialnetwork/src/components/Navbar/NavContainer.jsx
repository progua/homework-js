import { connect } from 'react-redux';
import Nav from './Nav';

const mapStateToProps = (state) => {
return {
navBar: state.navBar
}
}

const NavContainer = connect (mapStateToProps) (Nav)

export default NavContainer;