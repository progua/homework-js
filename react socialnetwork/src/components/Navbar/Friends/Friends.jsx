import s from './Friends.module.css';

const Friends = (props) => {
  return (
    <div className={s.avatar}>
      <a>
      <img src={props.avatar}></img>
      <p>{props.name}</p>
      </a>
    </div>
  )
}

export default Friends;