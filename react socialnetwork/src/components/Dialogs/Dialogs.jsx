import DialogItem from './DialogItem/DialogItem'
import s from './Dialogs.module.css'
import Message from './Message/Message'
import NewMessageContainer from './NewMessage/NewMessageContainer'

const Dialogs = (props) => {

  let state = props.dialogsPage;

  let dialogElement = state.dialogsData.map(dialog => <DialogItem name={dialog.name} id={dialog.id}></DialogItem>)

  let messageElement = state.messageData.map(message => <Message img={message.img} user={message.user} text={message.message}></Message>)

  return (
    <div className={s.dialogs}>
      <div className={s.users_list}>
        <ul>
          {dialogElement}
        </ul>
      </div>
      <div className={s.messages}>
        {messageElement}
        <NewMessageContainer></NewMessageContainer>
      </div>
    </div>
  )
}

export default Dialogs;