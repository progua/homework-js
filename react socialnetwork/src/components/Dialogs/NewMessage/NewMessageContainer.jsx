import { connect } from 'react-redux';
import { addMessageActionCreate } from '../../../redux/dialogs-reducer';
import NewMessage from './NewMessage';

const mapStateToProps = (state) => {
    return {
      dialogsPage: state.dialogsPage
    }
  }

const mapDispatchToProps = (dispatch) => {
    return {
        onAddMessage: (message) => {
            let action = addMessageActionCreate(message);
            dispatch(action)
        }
    }
}

const NewMessageContainer = connect(mapStateToProps, mapDispatchToProps)(NewMessage);

export default NewMessageContainer;