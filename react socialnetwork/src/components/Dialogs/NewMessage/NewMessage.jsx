import React from 'react';
import s from './NewMessage.module.css';




let NewMessage = (props) => {
    let newMessageElement = React.createRef();

    let addMessage = () => {
        let message = newMessageElement.current.value;
        props.onAddMessage(message)
        newMessageElement.current.value = ''
    }

    return (
        <div className={s.new_message}>
            <div>
                <textarea placeholder='Your message' ref={newMessageElement}></textarea>
            </div>
            <div className={s.button}>
                <button onClick={addMessage}>Send</button>
            </div>
        </div>
    )
}

export default NewMessage;