import s from '../Dialogs.module.css'

const Message = (props) => {
  return (
    <div className={s.message}>
      <div className={s.avatar}>
        <img src={props.img}></img>
        <p>{props.user}</p>
      </div>
      <div className={s.text}>{props.text}</div>      
    </div>
  )

}

export default Message;