import s from './ProfileInfo.module.css'

const ProfileInfo = () => {
  return (
    <div>
      <div className={s.wallpaper_img}>
        <img src='https://www.fxmag.pl/images/cache/article_header_filter/images/articles/co-to-jest-cosmos-atom-strefy-cosmos-cosmos-sdk-cosmos-hub-tendermint-dzialanie-kupno-najwazniejsze-informacje.jpg'></img>
      </div>
      <div className={s.profile}>
        <div className={s.profile_photo}>
          <img src='https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Calico_tabby_cat_-_Savannah.jpg/1200px-Calico_tabby_cat_-_Savannah.jpg'></img>
        </div>
        <div className={s.profile_info}>
          <h2>Cat Kitty</h2>
          <p>Birthday: 28.09</p>
          <p>City: Krakow</p>
          <p>Education: Front-End</p>
          <p>Web Site: www.kitty</p>
        </div>
      </div>
    </div>
  )
}

export default ProfileInfo;