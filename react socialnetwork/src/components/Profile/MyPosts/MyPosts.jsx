import React from 'react';
import s from './MyPosts.module.css'
import Post from './Post/Post';

const MyPosts = (props) => {

  let state = props.profilePage
  let postElement = state.postData.map(post => <Post key={post.id} message={post.message} likes={post.like}></Post>)

  let newPostElement = React.createRef();

  let addPost = () => {
    let text = newPostElement.current.value;
    props.onAddPost(text);
    newPostElement.current.value = ""
  }

  return (
    <div className={s.post}>
      <h3>My Posts</h3>
      <div className={s.new_post}>
        <div>
          <textarea placeholder='Your news' ref={newPostElement}></textarea>
        </div>
        <div className={s.button}>
          <button onClick={addPost}>Send</button>
        </div>
      </div>
      {postElement}
    </div>
  )
}

export default MyPosts;