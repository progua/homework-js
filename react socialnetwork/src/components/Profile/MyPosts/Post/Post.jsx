import s from './Post.module.css'

const Post = (props) => {
  return (
    <div className={s.other_posts}>
        <div className={s.post_item}>
          <div className={s.avatar}>
            <img src='https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Calico_tabby_cat_-_Savannah.jpg/1200px-Calico_tabby_cat_-_Savannah.jpg'></img>
          </div>
          <div className={s.text}>
            {props.message}
          </div>
          <span>like: {props.likes}</span>
        </div>        
      </div>
      )
}

export default Post;