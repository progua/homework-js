//ДЗ №3
const styles = ["Джаз", "Блюз"];
document.write("<p>Исходный: " + styles.join(", "));
let lenght = styles.push("Рок-н-ролл");
document.write("<p>После добавления: " + styles.join(", "));
styles.splice(Math.floor(styles.length / 2), 1, "Классика");
document.write("<p>После изменения среднего элемента: " + styles.join(", "));
let value = styles.shift();
document.write("<p>После удаления превого элемента: " + styles.join(", "));
document.write("<p>Удаленное значение: " + value);
styles.unshift("Реп", "Рэгги");
document.write("<p>С добавленными в начале: " + styles.join(", "));
document.write("<hr>");

//додаткове дз
// №1
const arr = [1,2,3], arr2 = ['a', 'b', 'c'];
let a = arr.concat(arr2);
document.write("<p> Задание 1 Объединенный массив: " + a.join(", "));
// №2
arr2.push(1,2,3);
document.write("<p> Задание 2 Массив с добавлением в конце: " + arr2.join(", "));
// №3
arr.reverse();
document.write("<p> Задание 3 Обратный массив: " + arr.join(", "));
// №4
const arr3 = [1,2,3];
arr3.push(4,5,6);
document.write("<p> Задание 4 Массив с добавлением в конце: " + arr3.join(", "));
// №5
const arr4 = [1,2,3];
arr4.unshift(4,5,6);
document.write("<p> Задание 5 Массив с добавлением в начало: " + arr4.join(", "));
// №6
const arr5 = ['js', 'css', 'jq'];
document.write("<p> Задание 6 Первый элемент: " + arr5[0]);
// №7
const arr6 = [1, 2, 3, 4, 5];
let b = arr6.slice(0,3);
document.write("<p> Задание 7 Возвращенные элементы: " + b.join(", "));
// №8
arr6.splice(1,2);
document.write("<p> Задание 8 Удаленные с помощью splice: " + arr6.join(", "));
// №9
const arr7 = [1, 2, 3, 4, 5];
arr7.splice(2,0,10);
document.write("<p> Задание 9 Добавленные с помощью splice: " + arr7.join(", "));
// №10
const arr8 = [3, 4, 1, 2, 7];
arr8.sort();
document.write("<p> Задание 10 Сортировка: " + arr8.join(", "));
// №11
const arr9 = ['Привет, ', 'мир', '!'];
document.write("<p> Задание 11: " + arr9.join(""));
// №12
arr9.splice(0,1, "Пока, ");
document.write("<p> Задание 12: " + arr9.join(""));
// №13
const arr10 = [1, 2, 3, 4, 5];
const arr11 = new Array(1, 2, 3, 4, 5);
// №14
const arr12 = {
    'ru':['голубой', 'красный', 'зеленый'],
    'en':['blue', 'red', 'green'],
};
document.write("<p>Задание 14: " + arr12['ru'][0]);
// №15
const arr13 = ['a', 'b', 'c', 'd'];
document.write("<p>Задание 15: " + arr13[0] + "+" + arr13[1] +"," +  " " + arr13[2] + "+" + arr13[3]);

// №16
let num = parseInt(prompt("Введіть число – величину масиву"));
const arr14 = [];
arr14.length = num;
for (let i = 0; i < num; i++) {
    arr14[i] = i;
}
document.write("<p>Задание 16: " + arr14.join(", "));

// №17
document.write("<p>Задание 17: " + "<br>");
for(let i = 0; i < arr14.length; i++) {
if (arr14[i] % 2 == 0) {
    document.write("Четные: <span style='background-color:red;'>" + arr14[i] + "</span>" + "<br>");
}
else {   
    document.write("<p> Нечетные: " + arr14[i] + "<br>");
}
}
// №18
const vegetables = ['Капуста', 'Репа', 'Редиска', 'Морковка'];
let str1 = vegetables.toString();
document.write("<p>Задание 18: " + str1); // "Капуста, Репа, Редиска, Морковка"
