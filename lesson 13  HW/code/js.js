const url = " https://swapi.dev/api/people",
    cardsArr = [];
localStorage.peoples = JSON.stringify([]);

function sendReqest(method, requestUrl) {
    return fetch(requestUrl).then(response => {
        return response.json()
    })
}
sendReqest("GET", url)
    .then(data => {
        showCards(data.results)
    })
    .catch(err => console.log(err))

function showCards(array) {
    array.forEach(element => {
        let card = {
            name: element.name,
            gender: element.gender,
            height: element.height,
            skinColor: element.skin_color,
            birthday: element.birth_year,
            homeworld: element.homeworld
        }
        show(card);
        cardsArr.push(card);

        function show() {
            const main = document.querySelector(".people_cards");
            let divCard = document.createElement("div"),
                headerCard = document.createElement("h2"),
                genderP = document.createElement("p"),
                heightP = document.createElement("p"),
                skinColorP = document.createElement("p"),
                birthdayP = document.createElement("p"),
                homeworldP = document.createElement("p"),
                buttonSubmit = document.createElement("button");

            headerCard.innerText = card.name;
            genderP.innerText = card.gender;
            heightP.innerText = card.height;
            skinColorP.innerText = card.skinColor;
            birthdayP.innerText = card.birthday;
            homeworldP.innerText = card.homeworld;
            buttonSubmit.innerText = "Save";

            divCard.classList.add("cards");
            buttonSubmit.type = "submit";
            buttonSubmit.classList.add("btnSubmit");
            buttonSubmit.setAttribute("id", element.name)


            main.append(divCard);
            divCard.append(headerCard, genderP, heightP, skinColorP, birthdayP, homeworldP, buttonSubmit);

        }
    }
    )
}

let buttonClick = document.querySelector(".people_cards");

let saveData = (e) => {
    let button = e.target;
    if (button.tagName != "BUTTON") return;

    cardsArr.forEach(element => {
    if(element.name === e.target.id) {
        let temp = JSON.parse(localStorage.peoples);
        temp.push(element);
        localStorage.peoples = JSON.stringify(temp);
    }}
    );
    button.classList.add("green")
}
buttonClick.addEventListener("click", saveData)







