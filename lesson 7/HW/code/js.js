//HW Lesson 7
// Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
// При вызове функция должна спросить у вызывающего имя и фамилию.
// Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
// Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
// Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). Вывести в консоль результат выполнения функции.

// При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
// Создать метод getAge() который будет возвращать сколько пользователю лет.
// Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, 
// соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).

function CreateNewUser(firstName, lastName, birthday) {
    this.name = firstName;
    this.lname = lastName;
    this.bday = birthday;
    }
    CreateNewUser.prototype.show = function() {
        return `${this.name} ${this.lname}`
    }
    CreateNewUser.prototype.getLogin = function() {
        return `${this.name[0]}${this.lname}`.toLowerCase();
    }
    CreateNewUser.prototype.getAge = function() {
    let today = new Date();
    let age = today.getFullYear() - this.bday.split(".")[2];
    let m = today.getMonth() - this.bday.split(".")[1];
    if (m < 0 || (m === 0 && today.getDate() < this.bday.split(".")[0])) 
    {
        age--;
    }
    return age;
    }
    CreateNewUser.prototype.getPassword = function() {
       return `${this.name[0].toUpperCase()}${this.lname.toLowerCase()}${this.bday.split(".")[2]}`
    }
const newUser = new CreateNewUser(prompt("Введіть своє ім'я"), prompt("Введіть своє прізвище"), prompt("Введіть дату народження у форматі dd.mm.yyyy", "дд.тт.уууу"));
console.log(newUser.show());
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());

// Написать функцию filterBy(), которая будет принимать в себя 2 аргумента. Первый аргумент - массив, который будет содержать в себе любые данные, второй аргумент - тип данных.
// Функция должна вернуть новый массив, который будет содержать в себе все данные, которые были переданы в аргумент, за исключением тех, тип которых был передан вторым аргументом. То есть, если передать массив ['hello', 'world', 23, '23', null], и вторым аргументом передать 'string', то функция вернет массив [23, null].

function filterBy (arr, type) {
    let newArr = arr.filter((item => typeof(item) !== type))
    console.log(newArr);
}

const arr = [null, 43, "cat", 355, "dog", "4", 712];
const type = 'number';

filterBy(arr,type);
