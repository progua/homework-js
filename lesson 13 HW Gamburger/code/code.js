const price = {
    small: 50,
    big: 100,
    cheese: 10,
    salat: 20,
    potato: 15,
    spice: 15,
    mayo: 20
}

const callories = {
    small: 20,
    big: 40,
    cheese: 20,
    salat: 5,
    potato: 10,
    spice: 0,
    mayo: 5
}

class Hamburger {
    constructor(size, stuffing) {
        this.size = size;
        this.stuffing = stuffing;
        this.price = Number();
        this.callories = Number();
        this.toppingArr = [];
    }

    getSize() {
        if (this.size === "small" || this.size === "big") {
            console.log(this.size)
        } else
            console.log(
                new Error(
                    "Ви вказали неправильний розмір. Введіть small або big"
                )
            );
    }

    getStuffing() {
        if (this.stuffing === "cheese" || this.stuffing === "salat" || this.stuffing === "potato") {
            console.log(this.stuffing)
        } else
            console.log(
                new Error(
                    "Ви вказали неправильну добавку. Введіть cheese, salat або potato"
                )
            );
    }

    addTopping(topping) {
        if (this.toppingArr.length >= 2) {
            console.log(
                new Error(
                    "Забагато топінгів"
                )
            );
        } else if (this.toppingArr[0] === topping && this.toppingArr.length !== 0) {
            console.log(
                new Error(
                    "Неможливо додати однаковий топінг"
                )
            );
        } else if (topping === "mayo" || topping === "spice") {
            this.toppingArr.push(topping);
        } else 
        console.log(
            new Error(
                "Ви вказали неправильний топінг. Введіть mayo або spice"
            )
        );
    }

    removeTopping(topping) {
        if (this.toppingArr[0] === topping || this.toppingArr[1] === topping) {
            let removeIndex = this.toppingArr.findIndex(item => item === topping);
            this.toppingArr.splice(removeIndex, 1);

        } else if (this.toppingArr.length === 0) {
            console.log(
                new Error(
                    "Немає топпінгів для видалення"
                )
            );
        } else
            console.log(
                new Error(
                    "Ви вказали неправильний топінг. Введіть mayo або spice"
                )
            );
    }

    calculateCalories() {
        if(this.callories !== 0) {
            this.callories = 0;
        }
        switch (this.size) {
            case "small":
                this.callories += callories.small;
                break;
            case "big":
                this.callories += callories.big;
                break;
        }

        switch (this.stuffing) {
            case "cheese":
                this.callories += callories.cheese;
                break;
            case "salat":
                this.callories += callories.salat;
                break;
            case "sptato":
                this.callories += callories.potato;
                break;
        }

        this.toppingArr.forEach(el => {
            switch (el) {
                case "mayo":
                    this.callories += callories.mayo;
                    break;
                case "spice":
                    this.callories += callories.spice;
                    break;
            }
        })
        console.log(this.callories)
    }

    totalPrice() {
        if(this.price !== 0) {
            this.price = 0;
        }

        switch (this.size) {
            case "small":
                this.price += price.small;
                break;
            case "big":
                this.price += price.big;
                break;
        }

        switch (this.stuffing) {
            case "cheese":
                this.price += price.cheese;
                break;
            case "salat":
                this.price += price.salat;
                break;
            case "sptato":
                this.price += price.potato;
                break;
        }

        this.toppingArr.forEach(el => {
            switch (el) {
                case "mayo":
                    this.price += price.mayo;
                    break;
                case "spice":
                    this.price += price.spice;
                    break;
            }
        })
        console.log(this.price)
    }
}


let gamburger = new Hamburger("big", "salat");

//Приклад виконання
gamburger.getSize();
gamburger.getStuffing();
gamburger.totalPrice();
gamburger.calculateCalories();
gamburger.addTopping("mayo");
gamburger.calculateCalories()
gamburger.addTopping("spice");
gamburger.totalPrice();
gamburger.removeTopping("spice");
gamburger.addTopping("mayo");





