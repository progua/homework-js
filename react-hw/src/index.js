import React from 'react';
import ReactDOM from 'react-dom';

const App = function () {
    return (
        <div>
            <Week></Week>
            <MonthZodiac></MonthZodiac>
        </div>
    )
}

const Week = function () {
    const days = ['Понеділк', 'Вівторок', 'Середа', 'Четвер', 'П`ятниця', 'Субота', 'Неділя'];
    const liDays = days.map((element, index) => <li key={index}>{element}</li>);

    return (
        <div>
            <TitleWeek></TitleWeek>
            <ul>
                {liDays}
            </ul>
        </div>
    )
}

const MonthZodiac = function () {

    const monthZodiac = [
        { id: 1, mounth: 'Січень', zodiac: 'Козеріг (1.01 - 20.01), Водолій (21.01 - 31.01)' },
        { id: 2, mounth: 'Лютий', zodiac: 'Водолій (1.02 - 18.02), Риби (19.02 - 28.02)' },
        { id: 3, mounth: 'Березень', zodiac: ' Риби (1.03 - 20.03), Овен (21.03 - 31.03)' },
        { id: 4, mounth: 'Квітень', zodiac: 'Овен (1.04 - 20.04), Телець (21.04 - 30.04)' },
        { id: 5, mounth: 'Травень', zodiac: ' Телець (1.05 - 21.05), Близнюки (22.05 - 31.0,5)' },
        { id: 6, mounth: 'Червень', zodiac: 'Близнюки (1.06 - 21.06), Рак (22.06 - 30.06)' },
        { id: 7, mounth: 'Липень', zodiac: 'Рак (1.07 - 21.07), Лев (22.07 - 31.07)' },
        { id: 8, mounth: 'Серпень', zodiac: 'Лев (1.08 - 23.08), Діва (24.08 - 31.08)' },
        { id: 9, mounth: 'Вересень', zodiac: 'Діва (1.09 - 23.09), Терези (24.09 - 30.09)' },
        { id: 10, mounth: 'Жовтень', zodiac: 'Терези (1.10 - 24.10), Скорпіон (25.10 - 31.10)' },
        { id: 11, mounth: 'Листопад', zodiac: 'Скорпіон (1.11 - 22.11), Стрілець (23.11 - 30.11)' },
        { id: 12, mounth: 'Грудень', zodiac: 'Стрілець (1.12 - 21.12), Козеріг (22.12 - 31.11)' }
    ]

    let mounthZodiacTable = monthZodiac.map(element => <tr key={element.id} >
        <td>{element.mounth}</td>
        <td>{element.zodiac}</td>
    </tr>)

    return (
        <div>
            <TitleZodiac></TitleZodiac>
            <table>
                <tbody>
                    <tr>
                        <th>Місяц</th>
                        <th>Знаки зодіаку</th>
                    </tr>
                    {mounthZodiacTable}
                </tbody>
            </table>
        </div>
    )
}

const TitleWeek = function () {
    return (
        <h2>Дні тижня</h2>
    )
}

const TitleZodiac = function () {
    return (
        <h2>Знаки зодіаку по місяцях</h2>
    )
}

ReactDOM.render(<App></App>, document.querySelector("#root"))