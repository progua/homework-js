// Завдання 1 ДЗ
function Human(name, surname, age) {
    this.name = name;
    this.surname = surname;
    this.age = age;
}
let people = [];
let human1 = new Human (
    prompt("Введіть ім'я"),
    prompt("Введіть прізвище"),
    parseInt(prompt("Введіть вік"))
);
let human2 = new Human (
    prompt("Введіть ім'я"),
    prompt("Введіть прізвище"),
    parseInt(prompt("Введіть вік"))
);
let human3 =  new Human (
    prompt("Введіть ім'я"),
    prompt("Введіть прізвище"),
    parseInt(prompt("Введіть вік"))
);
people.push(human1, human2, human3);

people.sort((a,b) => a.age - b.age);
for (let item of people) {
    document.write(`Ім'я: ${item.name}. <br> Прізвище: ${item.surname}. <br> Вік: ${item.age}. <hr>`)
}
// Завдання 2 ДЗ
function HumanPlayers(name, nickname, city, game) {
    this.name = name;
    this.nickname = nickname;
    this.city = city;
    this.game = game;
    this.show = function () {
        document.write(`Мене звати ${this.name} , але ви мене знаєте як ${this.nickname} , я з ${this.city} , я гравець у ${this.game} <br>`)
    } // метод екземпляра
}
let player1 = new HumanPlayers("Lena", "Foxy", "Kyiv", "Dota");
let player2 = new  HumanPlayers("Vlad", "Vaidor", "Lviv", "LOL");
let players3 = new HumanPlayers("Alice", "Hikki", "Kherson", "Dota");
let players = new Array(player1, player2, players3);

for (let item of players) {
         item.show()
}
HumanPlayers.prototype.whatGame = function(players) {
    let message = (this.game == "Dota") ? `Учасник турніру Dota` :  `Учасник турніру lol`;
    document.write(message);
} //метод прототипа

player1.whatGame (players);

document.write(`<hr>`)

// Класна робота
function Calculator () {
    this.read = function () {
        this.num1 = parseInt(prompt(`Введіть число`));
        this.num2 = parseInt(prompt(`Введіть друге число`));
        document.write (`${this.num1}, ${this.num2} <br>`);
    }
    this.sum = function () {
        document.write(`Сума: ${this.num1 + this.num2} <br>`) 
    } 
    this.mul = function () {
        document.write(`Сума: ${this.num1 * this.num2} <br>`) 
    }
}
let a = new Calculator;
a.read();
a.sum();
a.mul();
