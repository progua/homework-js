const item = document.querySelector(".item");
const placeholders = document.querySelectorAll(".placeholder")

placeholders.forEach((element) => {
element.addEventListener("dragover", dragover)
element.addEventListener("dragenter", dragenter)
element.addEventListener("dragleave", dragleave)
element.addEventListener("drop", dragdrop)
})

item.addEventListener("dragstart", dragstart)
item.addEventListener("dragend", dragend)

function dragstart(event) {
    event.target.classList.add("hold")
    setTimeout(() =>
        event.target.classList.add("hide"), 0
    )
}

function dragend(event) {
    event.target.classList.remove("hold", "hide")
}

function dragover(event) {
event.preventDefault()
}

function dragenter(event) {
    event.target.classList.add("hovered")
}

function dragleave(event) {
    event.target.classList.remove("hovered")
}

function dragdrop(event) {
    event.target.classList.remove("hovered")
    event.target.append(item)
}