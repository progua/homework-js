document.querySelector("input").onclick = function () {
    const fieldInput = document.createElement("input");
    const fieldButton = document.createElement("button");
    const container = document.createElement("div");

    fieldInput.type = "number";
    fieldInput.placeholder = "Введіть діаметр круга";
    fieldButton.innerHTML = "Намалювати";
    document.body.append(container);
    container.append(fieldInput);
    container.append(fieldButton);

 fieldButton.onclick = function () {
    const fieldCirkle = document.createElement ("div");
    document.body.append(fieldCirkle)
    const diametr = fieldInput.value + "px";

    for (let i = 0; i<100; i++) {
        const circle = document.createElement("div");
            circle.setAttribute("class", "circle");
            circle.style.width = diametr;
            circle.style.height = diametr;
            circle.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 50%, 50%)`;
            circle.style.borderRadius = "50%";
            circle.style.margin = "2px";
            circle.style.display = "inline-block";
            fieldCirkle.append(circle);
            circle.onclick = function () {
                circle.remove();
            }
    }
}
}