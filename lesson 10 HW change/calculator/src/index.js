window.addEventListener("DOMContentLoaded", () => {
    const keys = document.querySelector(".keys"),
        display = document.querySelector(".display input"),
        divM = document.querySelector(".m")

    let calculator = {
        firstNumber: "",
        secondNumber: "",
        arithmeticSign: "",
        mrc: "",
        result: ""
    }

    let mrcCounter = 0;

    keys.addEventListener("click", (e) => {
        validate(e.target.value);
    })

    const validate = (argument) => {
        if (/^[0-9.]$/.test(argument) && calculator.arithmeticSign == "") {
            if (calculator.result == "") {
                calculator.firstNumber += argument;
                mrcCounter = 0
            } else if (calculator.result !== "") {
                calculator.result = ""
                calculator.firstNumber = "";
                calculator.firstNumber += argument;
                mrcCounter = 0
            }
            if (/\.{2,}/.test(calculator.firstNumber)) {
                let replace = calculator.firstNumber.replace(/\.{2,}/g, ".");
                calculator.firstNumber = replace;
            } else if (/[0]{2,}\.+/.test(calculator.firstNumber)) {
                let replace0 = calculator.firstNumber.replace(/[0]{2,}\.+/g, "0.");
                calculator.firstNumber = replace0
            }
            display.value = calculator.firstNumber
        } else if (calculator.firstNumber !== "" && calculator.secondNumber == "" && /^[+/*-]$/.test(argument)) {
            calculator.arithmeticSign = argument;
            mrcCounter = 0
        } else if (calculator.firstNumber !== "" && calculator.arithmeticSign !== "" && /^[0-9.]$/.test(argument)) {
            calculator.secondNumber += argument;
            display.value = calculator.secondNumber
            mrcCounter = 0
            if (/\.{2,}/.test(calculator.secondNumber)) {
                let replace2 = calculator.secondNumber.replace(/\.{2,}/g, ".");
                calculator.secondNumber = replace2;
            } else if (/[0]{2,}\.+/.test(calculator.secondNumber)) {
                let replace02 = calculator.secondNumber.replace(/[0]{2,}\.+/g, "0.");
                calculator.secondNumber = replace02
            }
        } else if (calculator.secondNumber !== "" && calculator.arithmeticSign !== "" && /^[=+/*-]$/.test(argument)) {
            if (calculator.arithmeticSign == "+") {
                calculator.result = +calculator.firstNumber + +calculator.secondNumber;
                calculator.firstNumber = calculator.result;
                calculator.secondNumber = "";
                calculator.arithmeticSign = "";
                display.value = calculator.result
            } else if (calculator.arithmeticSign == "-") {
                calculator.result = +calculator.firstNumber - +calculator.secondNumber;
                calculator.firstNumber = calculator.result;
                calculator.secondNumber = "";
                calculator.arithmeticSign = "";
                display.value = calculator.result

            } else if (calculator.arithmeticSign == "*") {
                calculator.result = +calculator.firstNumber * +calculator.secondNumber;
                calculator.firstNumber = calculator.result;
                calculator.secondNumber = "";
                calculator.arithmeticSign = "";
                display.value = calculator.result

            } else if (calculator.arithmeticSign == "/") {
                calculator.result = +calculator.firstNumber / +calculator.secondNumber;
                calculator.firstNumber = calculator.result;
                calculator.secondNumber = "";
                calculator.arithmeticSign = "";
                display.value = calculator.result

            }
        } else if (argument == "m-" || argument == "m+") {
            if (argument == "m-") {
                calculator.mrc = -display.value;
                calculator.firstNumber = "";
                calculator.secondNumber = "";
            } else if (argument == "m+") {
                calculator.mrc = +display.value;
                calculator.firstNumber = "";
                calculator.secondNumber = "";
            }
            divM.innerHTML = "m"
        } else if (argument == "mrc") {
            if (mrcCounter == 0) {
                display.value = calculator.mrc
                mrcCounter++
            } else if (mrcCounter == 1) {
                clean();
            }

        } else if (argument == "C") {
            clean();

        }
    }
    let clean = () => {
        calculator.result = "";
        calculator.firstNumber = "";
        calculator.secondNumber = "";
        calculator.arithmeticSign = "";
        calculator.mrc = ""
        display.value = ""
        divM.innerHTML = ""
        mrcCounter = 0;
    }
})